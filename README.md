# Hallo Welt Paket

## Installation

1. venv erstellen und aktivieren, falls erforderlich  
1. Paket installieren  
    ```bash
    pip install git+https://gitlab.kit.edu/samuel.braun/hallo-welt-python.git
    ```
## Nutzung

1. Kommandozeile  
    ```bash
    python -c 'import hallowelt.hallo;hallowelt.hallo.say_hello()'
    ```
1. Im Skript  
    ```python
    import hallowelt.hallo as hey

    hey.say_hello()
    ```